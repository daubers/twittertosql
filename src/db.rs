
pub mod db {
    use postgres::{Client, NoTls, Error};
    use egg_mode::tweet::Tweet;
    use std::convert::TryFrom;

    pub fn make_client(host: &str, user: &str, passwd: &str, dbname: &str) -> Option<Client> {
        let mut client = Client::connect(&*format!("host={} user={} password={} dbname={}", host, user, passwd, dbname), NoTls).ok()?;
        return Some(client);
    }

    pub fn create_tables(client: &mut Client) -> () {
        let this_val = client.batch_execute("
            CREATE TABLE IF NOT EXISTS tweet (
                id                      SERIAL PRIMARY KEY,
                tweet_id                bigserial NOT NULL UNIQUE,
                created_at              TIMESTAMP WITH TIME ZONE,
                in_reply_to_user_id     bigserial NOT NULL,
                in_reply_to_status_id   bigserial NOT NULL,
                retweet_count           INT,
                tweet_text              TEXT,
                favourite_count         INT,
                coordinates_lat         FLOAT8,
                coordinates_long        FLOAT8,
                quoted_status_id        bigserial
            );
        ");
        if this_val.is_err() {
            println!("{:#?}", this_val);
        }
    }

    pub fn add_tweet(client: &mut Client, tweet: &Tweet) -> Result<u64, Error> {
        let statement = client.prepare(
            "INSERT INTO tweet (tweet_id, created_at, in_reply_to_user_id, in_reply_to_status_id, retweet_count, tweet_text, favourite_count, coordinates_lat, coordinates_long, quoted_status_id) \
                              VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10);")?;
        let tweet_id = i64::try_from(tweet.id).ok();
        let lat = match tweet.coordinates {
            None => 0.0,
            Some(_) => tweet.coordinates.unwrap().0
        };

        let long = match tweet.coordinates {
            None => 0.0,
            Some(_) => tweet.coordinates.unwrap().1
        };

        client.execute(&statement, &[
            &tweet_id.unwrap(),
            &tweet.created_at,
            &i64::try_from(tweet.in_reply_to_user_id.unwrap_or(0)).ok().unwrap_or(0),
            &i64::try_from(tweet.in_reply_to_status_id.unwrap_or(0)).ok().unwrap_or(0),
            &tweet.retweet_count,
            &tweet.text,
            &tweet.favorite_count,
            &lat,
            &long,
            &i64::try_from(tweet.quoted_status_id.unwrap_or(0)).ok().unwrap_or(0)])
    }
}