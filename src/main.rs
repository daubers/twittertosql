#![feature(allocator_api)]

mod db;

use egg_mode::error::{Result, Error};
use crate::db::db::add_tweet;
use egg_mode::{Response, Token};
use egg_mode::tweet::{Timeline, Tweet};
use std::alloc::Global;
use std::sync::Arc;
use postgres::Client;

#[tokio::main]
async fn main() -> Result<()> {
    let con_key = include_str!(".tts/consumer_key").trim();
    let con_secret = include_str!(".tts/consumer_secret").trim();

    let mut db_client = db::db::make_client(env!("POSTGRES_HOST"), env!("POSTGRES_USER"), env!("POSTGRES_PASS"), env!("POSTGRES_DB")).unwrap();
    db::db::create_tables(&mut db_client);

    let con_token = egg_mode::KeyPair::new(con_key, con_secret);

    println!("Pulling up the bearer token...");
    let token = egg_mode::auth::bearer_token(&con_token).await?;

    println!("Pulling up a user timeline...");
    let timeline =
        egg_mode::tweet::user_timeline("daubers14", true, true, &token).with_page_size(15);
    let (timeline, feed) = timeline.start().await.unwrap();
    let more = true;
    let max_id = process_tweet(&mut db_client, timeline.min_id, &token).await.ok_or_else(|| 0);
    println!("{}", format!("{:#?}", &max_id));
    Ok(())
}

async fn process_tweet(db_client: &mut Client, start_id: Option<u64>, token: &Token) -> Option<u64> {
        let mut timeline =
        egg_mode::tweet::user_timeline("daubers14", true, true, &token).with_page_size(15);
    let (_timeline, feed) = timeline.older(start_id).await.ok()?;
    for tweet in feed.response {
            let add_result = add_tweet(db_client, &tweet);
            println!("{}: {:#?}", tweet.id, add_result);

        }
    return _timeline.max_id;
}
